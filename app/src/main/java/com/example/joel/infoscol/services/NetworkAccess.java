package com.example.joel.infoscol.services;

import android.app.IntentService;
import android.content.Intent;

import com.example.joel.infoscol.objects.webService;


public class NetworkAccess extends IntentService {





    public NetworkAccess() {super("NetworkAccess");}


    @Override
    protected void onHandleIntent(Intent intent) {

        String subHost=intent.getStringExtra("subHost");
        String post=intent.getStringExtra("post");
        String filter=intent.getStringExtra("filter");


        String res= new webService(this).request(subHost, post);

        intent = new Intent();
        intent.setAction(filter);
        intent.putExtra("result", res);
        sendBroadcast(intent);
    }


}
