package com.example.joel.infoscol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.annonce;
import com.example.joel.infoscol.redefined.adapters.annonceList;

import java.util.ArrayList;

public class AnnoncesActivity extends StandartActivity {

    TextView title,TV;
    String res="[{\"date\":\"10/10/2017\",\"msg\":\"Votre fils Timamo Chrétien a été expulsé des cours pour non payement de la 2e tranche\",\"sender\":\"Le surveillant général\"},{\"date\":\"03/12/2017\",\"msg\":\"Pour la visite de l'évèque au sein de notre établissement le collège demande aux parents de bien vouloir préparer des offrandes\",\"sender\":\"Le Catéchiste\"}]";
    ArrayList<annonce> annonces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(7);
        final String student_name=getIntent().getStringExtra("student_name");
        final String annee=getIntent().getStringExtra("annee");
        setTitle(student_name + ' ' + annee);
        idb=new infoscolDB(this);
        annonces= idb.readAnnonces(annee);
        TV=(TextView)findViewById(R.id.TV_annonces_list_error);
        myListView=(ListView)findViewById(R.id.annonce_list);
        title=(TextView)findViewById(R.id.title_annonce);
        subHost="something.php";
        post="data=someData";
        filter="com.example.joel.infoscol.objects.testStudent";
        fill_myListView();
    }

    public void fill_myListView(){

        //objectsList =getData(objectsList,"annonce",res);
        annonceList AL= new annonceList(this,annonces);
        myListView.setAdapter(AL);
        if(myListView.getCount()<=0)
            TV.setText(getString(R.string.empty_list));
        title.setText(getString(R.string.annonces)+" ( "+ myListView.getCount() +" )");
    }

    private BroadcastReceiver onResponseReceive= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopService(network);
            unregisterReceiver(this);
            String res=intent.getStringExtra("result");
            // ICI gestion d'erreurs sur la reponse

            fill_myListView();



        }
    };
}
