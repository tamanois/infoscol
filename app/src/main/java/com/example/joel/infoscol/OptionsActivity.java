package com.example.joel.infoscol;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.joel.infoscol.redefined.adapters.customImageList;

import java.util.ArrayList;

public class OptionsActivity extends StandartActivity {
    TextView TV;
    ArrayList<String> options_tlist;
    ArrayList<Integer> img_option_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(3);
        final String student_name=getIntent().getStringExtra("student_name");
        final String annee=getIntent().getStringExtra("annee");
        final int id_eleve=getIntent().getIntExtra("id_eleve",0);

        setTitle(student_name+' '+annee);

        options_tlist=new ArrayList<>();
        img_option_list=new ArrayList<>();
<<<<<<< HEAD
        options_tlist.add(" Suivi frais de scolarité");
        img_option_list.add(R.drawable.pay_icon);
        options_tlist.add(" Note par matière");
        img_option_list.add(R.drawable.note_icon);
        options_tlist.add(" Moyenne évaluation");
        img_option_list.add(R.drawable.evaluation_icon);
        options_tlist.add(" Annonces");
=======
        options_tlist.add(getText(R.string.follow_fees).toString());
        img_option_list.add(R.drawable.pay_icon);
        options_tlist.add(getText(R.string.note_matiere).toString());
        img_option_list.add(R.drawable.note_icon);
        options_tlist.add(getText(R.string.evaluation_average).toString());
        img_option_list.add(R.drawable.evaluation_icon);
        options_tlist.add(getText(R.string.annonces).toString());
>>>>>>> dev
        img_option_list.add(R.drawable.icon_annonce);
        Log.e("ici", "1");
        myListView=(ListView)findViewById(R.id.options_list);
        Log.e("ici","2");
        TV=(TextView)findViewById(R.id.TV_options_list_error);
        Log.e("ici","3");
        fill_myListView(img_option_list,options_tlist);
        Log.e("ici", "4");

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    Intent i=new Intent(OptionsActivity.this,ScolariteActivity.class);
                    i.putExtra("student_name",student_name);
                    i.putExtra("id_eleve",id_eleve);
                    i.putExtra("annee",annee);
                    startActivity(i);

                }

                if(position==1){
                    Intent i=new Intent(OptionsActivity.this,NotesActivity.class);
                    i.putExtra("student_name",student_name);
                    i.putExtra("annee",annee);
                    i.putExtra("id_eleve",id_eleve);
                    startActivity(i);
                }
                if(position==2){
                    Intent i=new Intent(OptionsActivity.this,EvaluationsActivity.class);
                    i.putExtra("student_name",student_name);
                    i.putExtra("annee",annee);
                    i.putExtra("id_eleve",id_eleve);
                    startActivity(i);
                }
                if(position==3){
                    Intent i=new Intent(OptionsActivity.this,AnnoncesActivity.class);
                    i.putExtra("student_name",student_name);
                    i.putExtra("annee",annee);
                    i.putExtra("id_eleve",id_eleve);
                    startActivity(i);
                }
            }
        });
    }


    private void fill_myListView(ArrayList<Integer> imgListString,ArrayList<String> dataStringList){
        customImageList CIL=new customImageList(this,imgListString,dataStringList);
        //ArrayAdapter<String> AA=new ArrayAdapter(this,android.R.layout.simple_list_item_1,dataStringList);
        myListView.setAdapter(CIL);
        if(myListView.getCount()<=0)
            TV.setText(getText(R.string.empty_list));
    }
}
