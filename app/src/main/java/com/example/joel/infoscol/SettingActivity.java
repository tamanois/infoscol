package com.example.joel.infoscol;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SettingActivity extends StandartActivity {

    EditText ip,delais;
    Spinner spi_language;
    String [] language_list={"Français",
                            "English"};
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(8);
        setTitle(getString(R.string.action_settings));
        ip=(EditText)findViewById(R.id.ip_config);
        delais=(EditText)findViewById(R.id.delais);
        save=(Button)findViewById(R.id.save_config);
        spi_language=(Spinner)findViewById(R.id.language_select);
        ArrayAdapter<String> adap_language=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,language_list);
        spi_language.setAdapter(adap_language);
        String ip_address=loadSetting(this,"ip_address");
        if(ip_address!=null){
            ip.setText(ip_address);
        }

        String delay=loadSetting(this,"delais");
        if(delay!=null){

            try {
                int delais_maj=Integer.parseInt(delay);
                delais.setText(delay);
            } catch (NumberFormatException e) {

            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int lang_pos=spi_language.getSelectedItemPosition();
                if(lang_pos==0){
                    saveSetting(SettingActivity.this,"current_language","fr");
                }
                else if(lang_pos==1){
                    saveSetting(SettingActivity.this,"current_language","en");
                }
                saveSetting(SettingActivity.this, "ip_address", ip.getText().toString());
                saveSetting(SettingActivity.this, "delais", delais.getText().toString());
                Toast.makeText(SettingActivity.this, getString(R.string.config_saved), Toast.LENGTH_SHORT).show();

                new AlertDialog.Builder(SettingActivity.this)
                        .setTitle(getString(R.string.change_config))
                        .setMessage(getString(R.string.restart_app))
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    finish();
                                } catch (Exception e) {

                                }

                            }
                        }).show();


            }
        });
    }

}
