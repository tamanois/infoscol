package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class testStudent {
    String nom;
    int age;

    public testStudent(JSONObject jo){
        try {
            nom=jo.getString("nom");
            age=jo.getInt("age");
        }catch (Exception e){
            Log.e("testStudent class error",e.getMessage(),e.getCause());
        }


    }

    public String getNom() {
        return nom;
    }

    public int getAge() {
        return age;
    }
}
