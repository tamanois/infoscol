package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class testEvaluations {

    String matiere;
    int sequence;
    double note ;
    int coef;

    public testEvaluations(JSONObject jo){
        try{
            note=jo.getDouble("note");
            sequence=jo.getInt("sequence");
            matiere=jo.getString("matiere");
            coef=jo.getInt("coef");
        }catch (Exception e){
            Log.e("testEvals",e.getMessage(),e.getCause());
        }
    }

    public String getMatiere() {
        return matiere;
    }

    public int getSequence() {
        return sequence;
    }

    public double getNote() {
        return note;
    }

    public int getCoef() {
        return coef;
    }
}
