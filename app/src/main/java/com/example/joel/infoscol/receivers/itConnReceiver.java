package com.example.joel.infoscol.receivers;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.joel.infoscol.services.backgroundChecker;

public class itConnReceiver extends BroadcastReceiver {
    public itConnReceiver() {
    }
    Intent i;
    Context c;
    @Override
    public void onReceive(Context context, Intent intent) {
        i=intent;
        if (connected()) {
            //Toast.makeText(context, "connecté", Toast.LENGTH_LONG).show();
            if(!isServiceRunning(context, backgroundChecker.class))
            i=new Intent(context, backgroundChecker.class);
            i.putExtra("L2","on");
            context.startService(i);
        }
    }

    public  boolean connected(){
        if (i.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = i.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                return true;
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                return false;
            }

        }
         return false;
    }

    protected boolean isServiceRunning(Context c,Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
