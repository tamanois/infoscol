package com.example.joel.infoscol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.scolarite;

import java.util.ArrayList;

public class ScolariteActivity extends StandartActivity {
    TextView pension,reste,limite,total;
    LinearLayout LL_date,LL_montant;
    ArrayList<scolarite>scolarites;
    String res="[{\"pension\":\"212000\",\"date\":\"17/08/2017\",\"montant\":\"108500\",\"limite_versement\":\"08/10/2017\"},{\"pension\":\"212000\",\"date\":\"10/09/2017\",\"montant\":\"35000\",\"limite_versement\":\"08/10/2017\"}]";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(4);
        String student_name=getIntent().getStringExtra("student_name");
        String annee=getIntent().getStringExtra("annee");
        int id_eleve=getIntent().getIntExtra("id_eleve", 0);

        idb=new infoscolDB(this);
        scolarites=idb.readScolarites(id_eleve,annee);
        setTitle(student_name + ' ' + annee);
        LL_date=(LinearLayout)findViewById(R.id.date_column_items);
        LL_montant=(LinearLayout)findViewById(R.id.versement_column_items);
        pension=(TextView)findViewById(R.id.pension);
        reste=(TextView)findViewById(R.id.reste);
        total=(TextView)findViewById(R.id.total_verse);
        //limite=(TextView)findViewById(R.id.date_limite);
        fill_table();
    }

    public void fill_table(){

        boolean constDefined=false;
        String date_limite="";
        double somme=0,pension=0,reste;
        for(scolarite s:scolarites){
            if(!constDefined){
                pension=s.getPension();
                date_limite=s.getDate_reglement();
                constDefined=true;
            }
            float heigth=getResources().getDimension(R.dimen.eval_height);
            RelativeLayout rl=new RelativeLayout(this);
            rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            TextView d=new TextView(this);
            d.setText(s.getDate_reglement());
            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
            d.setLayoutParams(lp);
            d.setTypeface(Typeface.DEFAULT_BOLD);
            rl.addView(d);


            RelativeLayout rl1=new RelativeLayout(this);
            rl1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            TextView m=new TextView(this);
            m.setText(s.getMontant() + " FCFA");
            RelativeLayout.LayoutParams lp1=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp1.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
            m.setLayoutParams(lp1);
            m.setTypeface(Typeface.DEFAULT_BOLD);
            rl1.addView(m);





            somme+=s.getMontant();
            LL_date.addView(rl);
            LL_montant.addView(rl1);
        }
        total.setText(somme+" FCFA");
        reste=pension-somme;
//        this.limite.setText(date_limite);
        this.pension.setText(pension+" FCFA");
        this.reste.setText(reste+" FCFA");
    }

    private BroadcastReceiver onResponseReceive= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopService(network);
            unregisterReceiver(this);
            String res=intent.getStringExtra("result");
            // ICI gestion d'erreurs sur la reponse




        }
    };
}
