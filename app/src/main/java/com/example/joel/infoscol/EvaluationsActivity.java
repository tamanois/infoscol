package com.example.joel.infoscol;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.evaluation;

import java.util.ArrayList;

public class EvaluationsActivity extends StandartActivity {
    RelativeLayout rlSeq1,rlSeq2,rlSeq3,rlSeq4,rlSeq5,rlSeq6,rlTrim1,rlTrim2,rlTrim3;
    LinearLayout LL_matieres_item,LL_notes_item,LL_coef_items,LL_sequences_items;
    TextView moyenne;
    ScrollView sc_evaluation;
    ArrayList<evaluation> evalArray;
    ArrayList<String> list_matieres;
    ArrayList<Matiere> listMatieres;
    ArrayList<Integer> list_coef;
    ArrayList<evaluation> evaluations;


    String res="[{\"matiere\":\"Histoire\",\"sequence\":\"1\",\"note\":\"13\",\"coef\":\"1\"},{\"matiere\":\"Informatique\",\"sequence\":\"3\",\"note\":\"8\",\"coef\":\"2\"},{\"matiere\":\"Maths\",\"sequence\":\"2\",\"note\":\"14.5\",\"coef\":\"5\"},{\"matiere\":\"Maths\",\"sequence\":\"1\",\"note\":\"10.75\",\"coef\":\"5\"}]";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(6);
        String student_name=getIntent().getStringExtra("student_name");
        String annee=getIntent().getStringExtra("annee");
        final int id_eleve=getIntent().getIntExtra("id_eleve", 0);
        setTitle(student_name + ' ' + annee);
        idb=new infoscolDB(this);
        sc_evaluation=(ScrollView)findViewById(R.id.evaluations_scroll);
        evaluations= idb.readEvaluations(id_eleve,annee);
        rlSeq1=(RelativeLayout)findViewById(R.id.seq1);
        rlSeq1.setBackgroundColor(getResources().getColor(R.color.white));
        rlSeq2=(RelativeLayout)findViewById(R.id.seq2);
        rlSeq3=(RelativeLayout)findViewById(R.id.seq3);
        rlSeq4=(RelativeLayout)findViewById(R.id.seq4);
        rlSeq5=(RelativeLayout)findViewById(R.id.seq5);
        rlSeq6=(RelativeLayout)findViewById(R.id.seq6);
        rlTrim1=(RelativeLayout)findViewById(R.id.trim1);
        rlTrim2=(RelativeLayout)findViewById(R.id.trim2);
        rlTrim3=(RelativeLayout)findViewById(R.id.trim3);

        rlSeq1.setOnClickListener(rl_ocl);
        rlSeq2.setOnClickListener(rl_ocl);
        rlSeq3.setOnClickListener(rl_ocl);
        rlSeq4.setOnClickListener(rl_ocl);
        rlSeq5.setOnClickListener(rl_ocl);
        rlSeq6.setOnClickListener(rl_ocl);
        rlTrim1.setOnClickListener(rl_ocl);
        rlTrim2.setOnClickListener(rl_ocl);
        rlTrim3.setOnClickListener(rl_ocl);


        LL_matieres_item=(LinearLayout)findViewById(R.id.matiere_items);
        LL_notes_item=(LinearLayout)findViewById(R.id.notes_items);
        LL_coef_items=(LinearLayout)findViewById(R.id.coef_items);
        LL_sequences_items=(LinearLayout)findViewById(R.id.sequence_items);
        moyenne=(TextView)findViewById(R.id.TV_eval_moyenne);
        fill_table(res);
        fill_notes_seq(1);
    }

    View.OnClickListener rl_ocl=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                rlSeq1.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlSeq2.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlSeq3.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlSeq4.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlSeq5.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlSeq6.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlTrim1.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlTrim2.setBackgroundColor(getResources().getColor(R.color.light_blue));
                rlTrim3.setBackgroundColor(getResources().getColor(R.color.light_blue));
                RelativeLayout rl=(RelativeLayout)v;
                rl.setBackgroundColor(getResources().getColor(R.color.white));

                if(v.getId()==rlSeq1.getId()){
                    fill_notes_seq(1);
                }else if(v.getId()==rlSeq2.getId()){
                    fill_notes_seq(2);
                }else if(v.getId()==rlSeq3.getId()){
                    fill_notes_seq(3);
                }else if(v.getId()==rlSeq4.getId()){
                    fill_notes_seq(4);
                }else if(v.getId()==rlSeq5.getId()){
                    fill_notes_seq(5);
                }else if(v.getId()==rlSeq6.getId()){
                    fill_notes_seq(6);
                }else if(v.getId()==rlTrim1.getId()){
                    fill_notes_trim(1,2);
                }else if(v.getId()==rlTrim2.getId()){
                    fill_notes_trim(3,4);
                }else if(v.getId()==rlTrim3.getId()){
                    fill_notes_trim(5,6);
                }


            sc_evaluation.setSmoothScrollingEnabled(true);
            sc_evaluation.smoothScrollTo(0,0);
        }

    };

    public void fill_table(String res){

        list_matieres=new ArrayList<>();
        listMatieres=new ArrayList<>();
        list_coef=new ArrayList<>();
        evalArray=new ArrayList<>();
        objectsList =getData(objectsList,"testEvaluations",res);
        for(evaluation o:evaluations){

            evaluation tn=o;
            evalArray.add(tn);
            if(!list_matieres.contains(tn.getMatiere())){
                list_matieres.add(tn.getMatiere());
                list_coef.add(tn.getCoef());
                listMatieres.add(new Matiere(tn.getMatiere(),tn.getCoef()));
            }



        }
        fill_matiere();

    }

    public void fill_matiere(){
        int i=0;
        for(String str: list_matieres){
            float heigth=getResources().getDimension(R.dimen.eval_height);
            RelativeLayout rl=new RelativeLayout(this);
            rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            TextView tv=new TextView(this);
            tv.setText(str);
            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT,1);
            tv.setLayoutParams(lp);
            rl.addView(tv);
            LL_matieres_item.addView(rl);

            //coefficients
            rl=new RelativeLayout(this);
            rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            tv=new TextView(this);
            tv.setText(list_coef.get(i)+"");
            lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT,1);
            tv.setLayoutParams(lp);
            rl.addView(tv);
            LL_coef_items.addView(rl);
            i++;

        }


    }
    public void fill_notes_seq(int seq){
        LL_notes_item.removeAllViews();
        double moy;
        double total=0;
        double sCoef=0;
        for(Matiere mat: listMatieres){
            int cur=0;
            for (evaluation te: evalArray){

                if((te.getMatiere().equals(mat.getName())) && (te.getSequence()==seq)){

                    float heigth=getResources().getDimension(R.dimen.eval_height);
                    RelativeLayout rl=new RelativeLayout(this);
                    rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
                    TextView tv=new TextView(this);
                    tv.setText(te.getNote() + "");
                    if(te.getNote()<10)
                        tv.setTextColor(getResources().getColor(R.color.error_text));

                    total+=(te.getNote()*mat.getCoef());
                    sCoef+=mat.getCoef();
                    RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                    tv.setLayoutParams(lp);
                    tv.setTypeface(Typeface.DEFAULT_BOLD);
                    rl.addView(tv);
                    LL_notes_item.addView(rl);
                    break;

                }
                cur++;
            }
            if(cur==evalArray.size()){
                float heigth=getResources().getDimension(R.dimen.eval_height);
                RelativeLayout rl=new RelativeLayout(this);
                rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
                TextView tv=new TextView(this);
                tv.setText("-");
                tv.setTextColor(getResources().getColor(R.color.error_text));
                RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                tv.setLayoutParams(lp);
                tv.setTypeface(Typeface.DEFAULT_BOLD);
                rl.addView(tv);
                LL_notes_item.addView(rl);
            }
        }
        if(sCoef>0){
            moy=total/sCoef;
            moyenne.setText(round(moy,2)+"");
            if(moy<10)
                moyenne.setTextColor(getResources().getColor(R.color.error_text));
            else
                moyenne.setTextColor(getResources().getColor(R.color.white));
        }else{
            moyenne.setText("-");
                moyenne.setTextColor(getResources().getColor(R.color.white));
        }

    }

    public void fill_notes_trim(int seq1,int seq2){
        LL_notes_item.removeAllViews();
        double moy;
        double total=0;
        double sCoef=0;

        ArrayList<Double> seqA,seqB,trim;
        seqA=get_array_seq(seq1);
        seqB=get_array_seq(seq2);
        trim=new ArrayList<>();
        for(int i=0;i<seqA.size();i++){
            double d=0;
            if(seqA.get(i)!=(double)-100 && seqB.get(i)!=(double)-100){
                d=(seqA.get(i)+seqB.get(i))/2;
                trim.add(d);
            }
            else if(seqA.get(i)==(double)-100 && seqB.get(i)!=(double)-100){
                trim.add(seqB.get(i));
            }
            else if(seqA.get(i)!=(double)-100 && seqB.get(i)==(double)-100){
                trim.add(seqA.get(i));
            }else {
                trim.add((double)-100);
            }

        }

        for(int i=0;i<listMatieres.size();i++){
            if(trim.get(i)!=(double)-100){
                total+=trim.get(i)*listMatieres.get(i).getCoef();
                sCoef+=listMatieres.get(i).getCoef();
            }else{

            }
        }
        for(double n : trim){
            float heigth=getResources().getDimension(R.dimen.eval_height);
            RelativeLayout rl=new RelativeLayout(this);
            rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            TextView tv=new TextView(this);
            if (n != (double)-100) {
                tv.setText(n+"");
                if(n<10)
                    tv.setTextColor(getResources().getColor(R.color.error_text));
            }else{
                tv.setText("-");
            }

            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
            tv.setLayoutParams(lp);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            rl.addView(tv);
            LL_notes_item.addView(rl);
        }
        if(sCoef>0) {
            moy = total / sCoef;
            moyenne.setText(round(moy,2)+"");
            if(moy<10)
                moyenne.setTextColor(getResources().getColor(R.color.error_text));
            else
                moyenne.setTextColor(getResources().getColor(R.color.white));

        }else{
            moyenne.setText("-");
            moyenne.setTextColor(getResources().getColor(R.color.white));
        }

    }

    public ArrayList<Double> get_array_seq(int seq){
        LL_notes_item.removeAllViews();
        ArrayList<Double>listNotes=new ArrayList<>();

        for(Matiere mat: listMatieres){
            int cur=0;
            for (evaluation te: evalArray){

                if((te.getMatiere().equals(mat.getName())) && (te.getSequence()==seq)){

                    listNotes.add(te.getNote());
                    break;

                }
                cur++;
            }
            if(cur==evalArray.size()){
               listNotes.add((double)-100);
            }
        }
        return listNotes;

    }


    class Matiere{
        private String name;
        private int coef;

        public Matiere(String name, int coef) {
            this.name = name;
            this.coef = coef;
        }

        public String getName() {
            return name;
        }

        public int getCoef() {
            return coef;
        }
    }

}
