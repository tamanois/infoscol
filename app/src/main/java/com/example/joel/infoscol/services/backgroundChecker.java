package com.example.joel.infoscol.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.joel.infoscol.LoginActivity;
import com.example.joel.infoscol.R;
import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.annonce;
import com.example.joel.infoscol.objects.eleve;
import com.example.joel.infoscol.objects.evaluation;
import com.example.joel.infoscol.objects.inscription;
import com.example.joel.infoscol.objects.scolarite;
import com.example.joel.infoscol.objects.webService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class backgroundChecker extends IntentService {



    String subHost="getstudentinfos";
    String post;
    infoscolDB idb;
    static String version_name;


    public backgroundChecker() {
        super("backgroundChecker");
    }




    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version_name = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        boolean loop=true;
        int i=001;
        int timeout=20;
        while (loop){
            try {
                String delay=loadSetting(this,"delais");
                if(delay!=null) {

                    try {
                        int delais_maj = Integer.parseInt(delay);
                        timeout = delais_maj;
                    } catch (NumberFormatException nfe) {
                        timeout=20;
                    }
                }
                else{
                    timeout=20;
                }
                Thread.sleep((timeout*1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String u=loadSetting(this,"user");
            String p=loadSetting(this,"pass");
            if(u==null || p==null){

                return;

            }
            post="pseudo="+u+"&password="+p+"&version_name="+version_name+"&service=1";

            String res= new webService(this).request(subHost, post);
            String msg="";
            try {
                JSONObject o= new JSONObject(res);

                try {
                    String ver_check=o.getString("error");
                    if(ver_check.equals("outdated"))
                        make_outDate();
                    stopSelf();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                idb=new infoscolDB(this);
                idb.RAZ();
                JSONArray eleves=new JSONArray(o.getString("eleves"));
                for(i=0;i<eleves.length();i++){
                    eleve e =new eleve(eleves.getJSONObject(i));
                    Log.e("eleve",e.getNom());
                    idb.createEleve(e);
                }


                JSONArray inscriptions=new JSONArray(o.getString("inscriptions"));
                for(i=0;i<inscriptions.length();i++){
                    inscription ins =new inscription(inscriptions.getJSONObject(i));
                    idb.createInscription(ins);
                }
                JSONArray scolarites=new JSONArray(o.getString("reglement_scolarites"));
                for(i=0;i<scolarites.length();i++){
                    scolarite scol =new scolarite(scolarites.getJSONObject(i));
                    idb.createScolarite(scol);
                }
                JSONArray evaluations=new JSONArray(o.getString("evaluations"));
                for(i=0;i<evaluations.length();i++){
                    evaluation eval =new evaluation(evaluations.getJSONObject(i));
                    idb.createEvaluation(eval);
                }
                JSONArray annonces=new JSONArray(o.getString("annonces"));
                for(i=0;i<annonces.length();i++){
                    annonce eval =new annonce(annonces.getJSONObject(i));
                    idb.createAnnonce(eval);
                }





                idb.close();


                int ne=o.getInt("new_eleves");
                Log.e("new eleves",ne+"");
                if(ne>0){
                    msg+=getString(R.string.nouvel_eleve, ne)+ '\n';
                }

                ne=o.getInt("new_reglement_scolarites");
                Log.e("new scolarite",ne+"");
                if(ne>0){
                    msg+=getString(R.string.nouveau_payement, ne)+ '\n';
                }

                ne=o.getInt("new_evaluations");
                Log.e("new evaluation",ne+"");
                if(ne>0){
                    msg+=getString(R.string.nouvel_evaluation, ne)+ '\n';
                }

                ne=o.getInt("new_annonces");
                Log.e("new annonce",ne+"");
                if(ne>0){
                    msg+=getString(R.string.nouvel_annonce, ne)+ '\n';
                }


                if(msg.length()>0)
                    handleNewData(msg,i);

                i++;

            } catch (JSONException e) {
                Log.e("background checker",e.getMessage());
            }


        }


    }




    public String loadSetting(Context context,String name) {
        String value;
        SharedPreferences settings;
        settings = context.getSharedPreferences("settings", 0);
        value = settings.getString(name, null);
        return value;
    }
    public void saveSetting( Context context ,String name, String value) {
        SharedPreferences settings;
        settings = context.getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }



    void handleNewData(String res,int mNotificationId){
        Uri notification_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            try {
                Intent resultIntent = new Intent(this, LoginActivity.class);
                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)

                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("Infoscol")
                                .setContentText(res)
                                .setContentIntent(resultPendingIntent)
                                .setAutoCancel(true)
                                .setSound(notification_sound)
                                .setPriority(2)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(res));






                NotificationManager mNotifyMgr =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotifyMgr.notify(mNotificationId, mBuilder.build());


            }catch (Exception e){
                Log.e("notif error",e.getMessage());
            }
        }

        public void make_outDate(){
            saveSetting(this,"app_outDated","true");
        }






    @Override
    public void onDestroy() {
        Intent i = new Intent();
        i.setAction("zombieService");
        i.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(i);
    }
}
