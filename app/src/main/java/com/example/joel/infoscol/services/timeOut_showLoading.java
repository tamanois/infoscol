package com.example.joel.infoscol.services;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


public class timeOut_showLoading extends IntentService {
    boolean ended=false;
    String filter="";

    public timeOut_showLoading() {
        super("timeOut_showLoading");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        filter=intent.getStringExtra("filter");
        IntentFilter itf= new IntentFilter(filter);
        registerReceiver(refill, itf);


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!ended){
            Intent i=new Intent();
            i.setAction("com.app.show_loading");
            sendBroadcast(i);

        }



    }


    private BroadcastReceiver refill= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        try{
            unregisterReceiver(this);
        }catch (Exception e){

        }

            ended=true;
        }
    };
}
