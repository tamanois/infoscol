package com.example.joel.infoscol.redefined.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.joel.infoscol.R;

import java.util.ArrayList;


public class imageList extends ArrayAdapter{

    public final Activity context;
    ArrayList<String> eleve_list;
    ArrayList<String> sexe_list;
    public imageList(Activity context, ArrayList<String> eleve_list, ArrayList<String> sexe_list) {
        super(context, R.layout.image_listview, eleve_list);
        this.eleve_list=new ArrayList<>();
        this.sexe_list=sexe_list;
        this.context = context;

            this.eleve_list=eleve_list;

    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.image_listview, null, true);
        TextView tv_eleve = (TextView) rowView.findViewById(R.id.nom_eleve);
        ImageView IV=(ImageView)rowView.findViewById(R.id.imageList_img);

        tv_eleve.setText(eleve_list.get(position));

        if(sexe_list.get(position).toLowerCase().equals("f")){
            IV.setImageResource(R.drawable.girl);
        }else{
            IV.setImageResource(R.drawable.boy);
        }



        return rowView;
    }


}
