package com.example.joel.infoscol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.eleve;
import com.example.joel.infoscol.redefined.adapters.imageList;

import java.util.ArrayList;

public class ActivityStudent extends StandartActivity {

    TextView TV;
    ArrayList<String> name;
    ArrayList<String> sexe;
    ArrayList<eleve> eleves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(1);
        setTitle(getText(R.string.children));
        idb=new infoscolDB(this);
        eleves = idb.readAlleleves();
        myListView=(ListView)findViewById(R.id.student_list);
        TV=(TextView)findViewById(R.id.TV_student_list_error);
        fill_myListView();
        subHost="something.php";
        post="data=someData";
        filter="com.example.joel.infoscol.objects.testStudent";
        //pushRequest(onResponseReceive,subHost,post,filter);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=new Intent(ActivityStudent.this,DateActivity.class);
                String send_name=name.get(position);
                send_name=send_name.substring(send_name.indexOf("-")+1);
                i.putExtra("student_name",send_name);
                i.putExtra("id_eleve",eleves.get(position).getId());
                startActivity(i);
            }
        });

    }

    public ArrayList<String> get_student_name_list(ArrayList<eleve> at){
        ArrayList<String> name=new ArrayList<>();
        ArrayList<String> sexe=new ArrayList<>();
        int i=1;
        for(eleve e:at){
            name.add(" "+ e.getNom());
            sexe.add(e.getSexe());
            i++;
        }
        this.name=name;
        this.sexe=sexe;
        return name;
    }

    public void fill_myListView(){
        imageList IL=new imageList(this,get_student_name_list(eleves),sexe);
        //ArrayAdapter<String> AA=new ArrayAdapter(this,android.R.layout.simple_list_item_1,get_student_name_list(eleves));
        myListView.setAdapter(IL);
        if(myListView.getCount()<=0)
            TV.setText(getResources().getText(R.string.empty_list));
    }


    private BroadcastReceiver onResponseReceive= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopService(network);
            unregisterReceiver(this);
            String res=intent.getStringExtra("result");
            // ICI gestion d'erreurs sur la reponse

            fill_myListView();



        }
    };
}
