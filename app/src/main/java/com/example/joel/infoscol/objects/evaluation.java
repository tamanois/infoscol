package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class evaluation {

    int id;
    String annee_scolaire;
    int id_eleve;
    String matiere;
    int coef;
    double note ;
    int sequence;

    public evaluation(JSONObject jo){
        try{
            id=jo.getInt("id");
            note=jo.getDouble("note");
            annee_scolaire=jo.getString("annee_scolaire");
            annee_scolaire=annee_scolaire.replace("-"," - ");
            id_eleve=jo.getInt("id_eleve");
            coef=jo.getInt("coef");
            sequence=jo.getInt("numero_seq");
            matiere=jo.getString("matiere");
        }catch (Exception e){
            Log.e("testNote",e.getMessage(),e.getCause());
        }
    }

    public evaluation(int id, String annee_scolaire, int id_eleve, String matiere, int coef, double note, int sequence) {
        this.id = id;
        this.annee_scolaire = annee_scolaire;
        this.id_eleve = id_eleve;
        this.matiere = matiere;
        this.coef = coef;
        this.note = note;
        this.sequence = sequence;
    }

    public int getId() {
        return id;
    }

    public String getAnnee_scolaire() {
        return annee_scolaire;
    }

    public int getId_eleve() {
        return id_eleve;
    }

    public String getMatiere() {
        return matiere;
    }

    public int getCoef() {
        return coef;
    }

    public double getNote() {
        return note;
    }

    public int getSequence() {
        return sequence;
    }
}
