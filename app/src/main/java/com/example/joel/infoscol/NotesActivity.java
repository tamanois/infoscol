package com.example.joel.infoscol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.evaluation;

import java.util.ArrayList;

public class NotesActivity extends StandartActivity {
    ArrayList<String> list_matieres;
    ScrollView sc_notes;
    ArrayList<Integer> list_sequences;
    LinearLayout LL_matiere,LL_sequence,LL_note;
    TextView moyenne;
    ArrayList<evaluation> evaluations;
    String res="[{\"matiere\":\"Histoire\",\"sequence\":\"1\",\"note\":\"13\"},{\"matiere\":\"Informatique\",\"sequence\":\"3\",\"note\":\"8\"},{\"matiere\":\"Maths\",\"sequence\":\"2\",\"note\":\"14.5\"},{\"matiere\":\"Maths\",\"sequence\":\"1\",\"note\":\"10.75\"}]";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(5);
        String student_name=getIntent().getStringExtra("student_name");
        String annee=getIntent().getStringExtra("annee");
        final int id_eleve=getIntent().getIntExtra("id_eleve", 0);
        setTitle(student_name + ' ' + annee);
        idb=new infoscolDB(this);
        sc_notes=(ScrollView)findViewById(R.id.notes_scroll);
        evaluations= idb.readEvaluations(id_eleve,annee);
        moyenne=(TextView)findViewById(R.id.TV_notes_moyenne);
        LL_matiere=(LinearLayout)findViewById(R.id.matiere_column);
        LL_sequence=(LinearLayout)findViewById(R.id.sequence_column);
        LL_note=(LinearLayout)findViewById(R.id.notes_column);
        fill_table();

    }

    public void fill_table(){

        list_matieres=new ArrayList<>();
        list_sequences=new ArrayList<>();
        boolean constDefined=false;
        for(evaluation o:evaluations){

                    evaluation tn=o;
            if(!constDefined){
                constDefined=true;
            }
            if(!list_matieres.contains(tn.getMatiere())){
                list_matieres.add(tn.getMatiere());
            }
            if(!list_sequences.contains(tn.getSequence())){
                list_sequences.add(tn.getSequence());
            }
        }

        list_sequences=up_sort(list_sequences);
        float heigth=getResources().getDimension(R.dimen.eval_height);

        for(String st:list_matieres){

            RelativeLayout rl=new RelativeLayout(this);
            rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            TextView tv=new TextView(this);
            tv.setText(st);
            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_VERTICAL, 1);
            tv.setLayoutParams(lp);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            rl.addView(tv);

            LL_matiere.addView(rl);
            rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0;i<LL_matiere.getChildCount();i++){
                        LL_matiere.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.light_blue));
                    }
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    TextView tv = (TextView) ((RelativeLayout)v).getChildAt(0);
                    fill_notes(tv.getText().toString());
                    sc_notes.setSmoothScrollingEnabled(true);
                    sc_notes.smoothScrollTo(0,0);


                }
            });
        }

        for(int s:list_sequences){

            RelativeLayout rl=new RelativeLayout(this);
            rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
            TextView tv=new TextView(this);
            tv.setText(s + "");
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
            tv.setLayoutParams(lp);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            rl.addView(tv);

            LL_sequence.addView(rl);
        }
        try{
            View v=LL_matiere.getChildAt(0);
            v.setBackgroundColor(getResources().getColor(R.color.white));
            TextView tv = (TextView) ((RelativeLayout)v).getChildAt(0);
            fill_notes(tv.getText().toString());
        }catch (Exception e){

        }

    }
    public  void fill_notes(String matiere) {
        LL_note.removeAllViews();
        float heigth=getResources().getDimension(R.dimen.eval_height);
        double somme=0;
        int n=0;
        for(int i=0;i<list_sequences.size();i++)
        for (evaluation o : evaluations) {
            evaluation tn = o;
            if((tn.getSequence()==(i+1)) && tn.getMatiere().equals(matiere) ){
                RelativeLayout rl=new RelativeLayout(this);
                rl.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) heigth));
                TextView tv=new TextView(this);
                tv.setText(tn.getNote() + "");
                tv.setTypeface(Typeface.DEFAULT_BOLD);
                RelativeLayout.LayoutParams lp=new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
                tv.setLayoutParams(lp);
                tv.setTypeface(Typeface.DEFAULT_BOLD);
                rl.addView(tv);


                tv.setText(tn.getNote() + "");
                somme+=tn.getNote();
                LL_note.addView(rl);
                n++;
                break;
            }
        }
        if(n>0)
            this.moyenne.setText(round(somme/(double)n,2)+"");
        else
            this.moyenne.setText("--");
    }
    private BroadcastReceiver onResponseReceive= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopService(network);
            unregisterReceiver(this);
            String res=intent.getStringExtra("result");
            // ICI gestion d'erreurs sur la reponse

            //fill_table(res);



        }
    };

    public ArrayList up_sort(ArrayList<Integer> ali){
       int n=ali.size();
        int i=0,j=0;
        int a;
        while((i<n-1)){
            j=i+1;
            while(j<n){
                if(ali.get(j)<ali.get(i)){
                    a=ali.get(j);
                    ali.set(j,ali.get(i));
                    ali.set(i,a);

                }
                j++;
            }
            i++;
        }
        return ali;
    }
}
