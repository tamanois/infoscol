package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class inscription {

    int id;
    String annee_scolaire;
    String classe;
    int id_eleve;
    double montant;




    public inscription(JSONObject jo){
        try{
            id=jo.getInt("id");
            classe=jo.getString("classe");
            annee_scolaire=jo.getString("annee_scolaire");
            annee_scolaire=annee_scolaire.replace("-"," - ");
            id_eleve=jo.getInt("id_eleve");
            montant=jo.getDouble("montant");
        }catch (Exception e){
            Log.e("inscription",e.getMessage(),e.getCause());
        }
    }

    public inscription(int id, String annee_scolaire, String classe, int id_eleve, double montant) {
        this.id = id;
        this.annee_scolaire = annee_scolaire;
        this.classe = classe;
        this.id_eleve = id_eleve;
        this.montant = montant;
    }

    public int getId() {
        return id;
    }

    public String getAnnee_scolaire() {
        return annee_scolaire;
    }

    public String getClasse() {
        return classe;
    }

    public int getId_eleve() {
        return id_eleve;
    }

    public double getMontant() {
        return montant;
    }
}
