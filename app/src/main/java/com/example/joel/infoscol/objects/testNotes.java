package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class testNotes {

    String matiere;
    int sequence;
    double note ;

    public testNotes(JSONObject jo){
        try{
            note=jo.getDouble("note");
            sequence=jo.getInt("sequence");
            matiere=jo.getString("matiere");
        }catch (Exception e){
            Log.e("testNote",e.getMessage(),e.getCause());
        }
    }

    public String getMatiere() {
        return matiere;
    }

    public int getSequence() {
        return sequence;
    }

    public double getNote() {
        return note;
    }

}
