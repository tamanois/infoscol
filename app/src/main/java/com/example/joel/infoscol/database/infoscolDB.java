package com.example.joel.infoscol.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.joel.infoscol.objects.annonce;
import com.example.joel.infoscol.objects.eleve;
import com.example.joel.infoscol.objects.evaluation;
import com.example.joel.infoscol.objects.inscription;
import com.example.joel.infoscol.objects.scolarite;

import java.util.ArrayList;

/**
 * Created by Joel on 31/08/2017.
 */
public class infoscolDB extends SQLiteOpenHelper {

    static int DB_version=1;
    static String DB_name="infoscol";



    public infoscolDB(Context c)
    {
        super(c, DB_name, null, DB_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String T_eleve="CREATE TABLE `eleves` (\n" +
                "  `id` int(10)  NOT NULL,\n" +
                "  `matricule` varchar(191) NOT NULL,\n" +
                "  `nom` varchar(191) NOT NULL,\n" +
                "  `sexe` varchar(191) NOT NULL,\n" +
                "  `id_parent` int(11) NOT NULL,\n" +
                "  `date_naissance` date NOT NULL,\n" +
                "  `created_at` timestamp NULL DEFAULT NULL,\n" +
                "  `updated_at` timestamp NULL DEFAULT NULL\n" +
                ");";
        String T_inscription="CREATE TABLE `inscriptions` (\n" +
                "  `id` int(10)  NOT NULL,\n" +
                "  `annee_scolaire` varchar(191) NOT NULL,\n" +
                "  `id_eleve` int(11) NOT NULL,\n" +
                "  `classe` varchar(191)  NOT NULL,\n" +
                "  `montant` double NOT NULL" +
                ");";
        String T_scolarite="CREATE TABLE `scolarites` (\n" +
                "  `id` int(10)  NOT NULL,\n" +
                "  `annee_scolaire` varchar(191) NOT NULL,\n" +
                "  `id_eleve` int(11) NOT NULL,\n" +
                "  `date_reglement` date NOT NULL,\n" +
                "  `montant` double NOT NULL" +
                ");";
        String T_evaluation="CREATE TABLE `evaluations` (\n" +
                "  `id` int(10)  NOT NULL,\n" +
                "  `annee_scolaire` varchar(191) NOT NULL,\n" +
                "  `id_eleve` varchar(191) NOT NULL,\n" +
                "  `matiere` varchar(191) NOT NULL,\n" +
                "  `note` double NOT NULL,\n" +
                "  `coef` int(11) NOT NULL,\n" +
                "  `numero_seq` int(11) NOT NULL" +

                ");";
        String T_annonce="CREATE TABLE `annonces` (\n" +
                "  `id` int(10)  NOT NULL,\n" +
                "  `annee_scolaire` varchar(191)  NOT NULL,\n" +
                "  `code_etablissement` int(11) NOT NULL,\n" +
                "  `destinataires` varchar(191) DEFAULT NULL,\n" +
                "  `intitule_annonce` varchar(191)  NOT NULL,\n" +
                "  `date_annonce` date NOT NULL, \n" +
                "  `emetteur` varchar(191) NOT NULL, \n" +
                "  `created_at` timestamp   DEFAULT NULL" +
                ");";

        db.execSQL(T_eleve);
        db.execSQL(T_inscription);
        db.execSQL(T_scolarite);
        db.execSQL(T_evaluation);
        db.execSQL(T_annonce);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String D_eleve="DROP TABLE IF EXISTS eleves";
        db.execSQL(D_eleve);
        onCreate(db);
    }

    public void RAZ() {
        SQLiteDatabase db=this.getWritableDatabase();
        String RAZ="DELETE FROM  eleves;";
        String RAZ1="DELETE FROM inscriptions;";
        String RAZ2="DELETE FROM scolarites;";
        String RAZ3="DELETE FROM evaluations;";
        String RAZ4="DELETE FROM annonces;";
        try {
            db.execSQL(RAZ);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            db.execSQL(RAZ1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            db.execSQL(RAZ2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            db.execSQL(RAZ3);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            db.execSQL(RAZ4);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //eleve
    public void createEleve(eleve e){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues val= new ContentValues();
        val.put("id",e.getId());
        val.put("matricule",e.getMatricule());
        val.put("nom",e.getNom());
        val.put("sexe",e.getSexe());
        val.put("id_parent",e.getId_parent());
        val.put("date_naissance", e.getDate_naissance());
        db.insert("eleves", null, val);
    }
    public ArrayList readAlleleves(){
        ArrayList<eleve> eleves=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT * FROM eleves";
        Cursor cur=db.rawQuery(query,null);
        while (cur.moveToNext()){
            eleve e=new eleve(cur.getInt(0),cur.getString(1),cur.getString(2),cur.getString(3),cur.getInt(4),cur.getString(5));
            eleves.add(e);
        }

        return eleves;
    }
    public eleve readEleve(int id){
        eleve e;
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT * FROM eleves WHERE id="+id;
        Cursor cur=db.rawQuery(query,null);
        if (cur.moveToFirst()){
            e=new eleve(cur.getInt(0),cur.getString(1),cur.getString(2),cur.getString(3),cur.getInt(4),cur.getString(5));
        }else{
            e=null;
        }

        return e;
    }
    //inscription
    public void createInscription(inscription i){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues val= new ContentValues();
        val.put("id",i.getId());
        val.put("annee_scolaire",i.getAnnee_scolaire());
        val.put("classe",i.getClasse());
        val.put("id_eleve",i.getId_eleve());
        val.put("montant",i.getMontant());
        db.insert("inscriptions",null,val);
    }
    public ArrayList readInscriptionsByEleve(int id_eleve){
        ArrayList<inscription> inscriptions=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT * FROM inscriptions WHERE id_eleve="+id_eleve;
        Cursor cur=db.rawQuery(query,null);
        while (cur.moveToNext()){
            inscription i=new inscription(cur.getInt(0),cur.getString(1),cur.getString(3),cur.getInt(2),cur.getDouble(4));
            inscriptions.add(i);
        }

        return inscriptions;
    }
    //scolarites
    public void createScolarite(scolarite s){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues val= new ContentValues();
        val.put("id", s.getId());
        val.put("annee_scolaire", s.getAnnee_scolaire());
        val.put("id_eleve",s.getId_eleve());
        val.put("date_reglement",s.getDate_reglement());
        val.put("montant", s.getMontant());
        db.insert("scolarites",null,val);
    }
    public ArrayList readScolarites(int id_eleve,String annee_scolaire){
        ArrayList<scolarite> scolarites=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT s.id,s.annee_scolaire,s.id_eleve,s.date_reglement,s.montant,i.montant AS pension FROM scolarites s,inscriptions i WHERE s.id_eleve="+id_eleve+" AND s.annee_scolaire='"+annee_scolaire+"' AND i.annee_scolaire=s.annee_scolaire AND i.id_eleve=s.id_eleve";
        Cursor cur=db.rawQuery(query,null);
        while (cur.moveToNext()){
            scolarite s=new scolarite(cur.getInt(0),cur.getString(1),cur.getInt(2),cur.getString(3),cur.getDouble(4),cur.getDouble(5));
            scolarites.add(s);
        }

        return scolarites;
    }
    //evaluations
    public void createEvaluation(evaluation e){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues val= new ContentValues();
        val.put("id",e.getId());
        val.put("annee_scolaire",e.getAnnee_scolaire());
        val.put("id_eleve",e.getId_eleve());
        val.put("matiere",e.getMatiere());
        val.put("note", e.getNote());
        val.put("coef",e.getCoef());
        val.put("numero_seq",e.getSequence());
        db.insert("evaluations", null, val);
    }
    public ArrayList readEvaluations(int id_eleve,String annee_scolaire){
        ArrayList<evaluation> evaluations=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT * FROM evaluations e WHERE e.id_eleve="+id_eleve+" AND e.annee_scolaire='"+annee_scolaire+"'";
        Cursor cur=db.rawQuery(query,null);
        while (cur.moveToNext()){
            evaluation s=new evaluation(cur.getInt(0),cur.getString(1),cur.getInt(2),cur.getString(3),cur.getInt(5),cur.getDouble(4),cur.getInt(6));
            evaluations.add(s);
        }

        return evaluations;
    }

    public void createAnnonce(annonce a){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues val= new ContentValues();
        val.put("id", a.getId());
        val.put("annee_scolaire", a.getAnnee_scolaire());
        val.put("code_etablissement", a.getCode_etablissement());
        val.put("destinataires", a.getClasse());
        val.put("intitule_annonce", a.getMsg());
        val.put("date_annonce", a.getDate());
        val.put("emetteur", a.getSender());
        val.put("created_at", a.getCreated_at());
        db.insert("annonces", null, val);
    }
    public ArrayList readAnnonces(String annee_scolaire){
        ArrayList<annonce> annonces=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        String query="SELECT * FROM annonces WHERE annee_scolaire='"+annee_scolaire+"' ORDER BY created_at DESC";
        Cursor cur=db.rawQuery(query,null);
        while (cur.moveToNext()){
            annonce s=new annonce(cur.getInt(0),cur.getString(1),cur.getInt(2),cur.getString(3),cur.getString(4),cur.getString(5),cur.getString(6));
            annonces.add(s);
        }

        return annonces;
    }



}
