package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class eleve {
    int id;
    String matricule;
    String nom;
    String sexe;
    int id_parent;
    String date_naissance;

    public eleve(JSONObject jo){
        try {
            id=jo.getInt("id");
            matricule=jo.getString("matricule");
            nom=jo.getString("nom");
            sexe=jo.getString("sexe");
            id_parent=jo.getInt("id_parent");
            date_naissance=jo.getString("date_naissance");
        }catch (Exception e){
            Log.e("eleve class error",e.getMessage(),e.getCause());
        }


    }

    public eleve(int id, String matricule, String nom,String sexe, int id_parent, String date_naissance) {
        this.id = id;
        this.matricule = matricule;
        this.nom = nom;
        this.sexe=sexe;
        this.id_parent = id_parent;
        this.date_naissance = date_naissance;
    }

    public int getId() {
        return id;
    }

    public String getMatricule() {
        return matricule;
    }

    public String getNom() {
        return nom;
    }

    public int getId_parent() {
        return id_parent;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public String getSexe() {
        return sexe;
    }
}
