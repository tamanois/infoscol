package com.example.joel.infoscol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.services.NetworkAccess;
import com.example.joel.infoscol.services.loadingService;
import com.example.joel.infoscol.services.timerService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;

public class StandartActivity extends AppCompatActivity {
    ViewFlipper VF;
    protected String subHost, post, filter;
    protected ArrayList<Object> objectsList;
    protected ListView myListView;
    protected Context c;
    protected Intent network;
    protected infoscolDB idb;
    RelativeLayout container;
    RelativeLayout anim_back;
<<<<<<< HEAD
=======
    public boolean opStarted=false;
    GifImageView anim;
    protected String programError="com.hotplaces.program.error";
>>>>>>> dev



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        try {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        } catch (Exception e) {
            Log.e("standart", e.getStackTrace().toString());
        }

        super.onPostCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if( id==android.R.id.home) {
            finish();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_annonces) {
            startActivity(new Intent(this,send_annoncesActivity.class));
            return true;
        }
        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setVF(int pos) {
        VF = (ViewFlipper) findViewById(R.id.content_flipper);
        VF.setDisplayedChild(pos);
    }

    protected ArrayList getData(ArrayList<Object> l,String className, String res) {
        final String objectsPackageName="com.example.joel.infoscol.objects.";
        l = new ArrayList<>();

        try {
            className=objectsPackageName+className;
            Class<?> cl=Class.forName(className);
            Constructor<?> co=cl.getConstructor(JSONObject.class);
            Object obj;
            int i = 0;
            JSONArray jso = new JSONArray(res);
            JSONObject o;

            int n = jso.length();
            while (i < n) {
                o = jso.getJSONObject(i);
                obj=co.newInstance(o);
                l.add(obj);
                i++;
            }


        } catch (Exception e) {

                //programmedError(1000);            }
                Log.e("get error",e.getMessage(),e.getCause());

        }
        return l;
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public void saveSetting( Context context ,String name, String value) {
        SharedPreferences settings;
        settings = context.getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public String loadSetting(Context context,String name) {
        String value;
        SharedPreferences settings;
        settings = context.getSharedPreferences("settings", 0);
        value = settings.getString(name, null);
        return value;
    }

    protected void pushRequest(BroadcastReceiver BR,String subHost,String post,String filter,Context c){

        IntentFilter itf= new IntentFilter(filter);
        c.registerReceiver(BR, itf);
        network=new Intent(c,NetworkAccess.class);
        network.putExtra("subHost", subHost);
        network.putExtra("post",post);
        network.putExtra("filter", filter);
        c.startService(network);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            idb.close();

        }catch (Exception e){

        }
    }




    public void startLoading() {

        container = (RelativeLayout) findViewById(R.id.main_container);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, 1);
        anim_back = new RelativeLayout(this);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        anim_back.setLayoutParams(params1);
        anim_back.setBackgroundColor(getResources().getColor(R.color.loading_back));
        container.addView(anim_back);


        anim = new GifImageView(this);
        anim.setImageResource(R.drawable.loading_anim);

        anim.setLayoutParams(params);

        anim.setScaleType(ImageView.ScaleType.FIT_XY);

        container.addView(anim);
        anim_back.bringToFront();
        anim.bringToFront();

        IntentFilter itf = new IntentFilter("com.hotplaces.networking.finished");
        registerReceiver(finishLoadingReceiver, itf);
        Intent i = new Intent(this, loadingService.class);
        startService(i);
    }

    public void programmedError(int timer){
        registerReceiver(programmedError,new IntentFilter(programError));
        Intent i=new Intent(this,timerService.class);
        i.putExtra("filter",programError);
        i.putExtra("time", timer);
        startService(i);

    }
    protected BroadcastReceiver programmedError =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    unregisterReceiver(this);
                   // Intent i=new Intent(context, ActivityConnectError.class);
                   // Intent last=getIntent();
                   // i.putExtra("intent",last);
                  //  startActivity(i);
                   // finish();
                }
            };


    private BroadcastReceiver finishLoadingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(this);
            finishLoading();
        }
    };

    public void finishLoading() {
        container.removeView(anim_back);
        container.removeView(anim);
    }
    public BroadcastReceiver refill2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(this);
            if (opStarted && loadingService.networking) {
                // loadingService.networking = true;
                startLoading();
                opStarted = false;
            }

        }
    };
}