package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class scolarite {

    int id;
    String annee_scolaire;
    int id_eleve;
    String date_reglement;
    double montant ;
    double pension;

    public scolarite(JSONObject jo){
        try{
            id=jo.getInt("id");
            id_eleve=jo.getInt("id_eleve");
            annee_scolaire=jo.getString("annee_scolaire");
            annee_scolaire=annee_scolaire.replace("-"," - ");
            montant=jo.getDouble("montant");
            date_reglement=jo.getString("date_reglement");
        }catch (Exception e){
            Log.e("scolarite",e.getMessage(),e.getCause());
        }
    }

    public scolarite(int id, String annee_scolaire, int id_eleve, String date_reglement, double montant,double pension) {
        this.id = id;
        this.annee_scolaire = annee_scolaire;
        this.id_eleve = id_eleve;
        this.date_reglement = date_reglement;
        this.montant = montant;
        this.pension=pension;
    }

    public int getId() {
        return id;
    }

    public String getAnnee_scolaire() {
        return annee_scolaire;
    }

    public int getId_eleve() {
        return id_eleve;
    }

    public String getDate_reglement() {
        return date_reglement;
    }

    public double getMontant() {
        return montant;
    }

    public double getPension(){return pension;};
}
