package com.example.joel.infoscol;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.annonce;
import com.example.joel.infoscol.objects.eleve;
import com.example.joel.infoscol.objects.evaluation;
import com.example.joel.infoscol.objects.inscription;
import com.example.joel.infoscol.objects.scolarite;
import com.example.joel.infoscol.services.backgroundChecker;
import com.example.joel.infoscol.services.loadingService;
import com.example.joel.infoscol.services.timeOut_showLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class LoginActivity extends StandartActivity {
    Button conn;
    EditText user,pass;
    TextView tv_error;
    int defaultColorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        String current_language=loadSetting(this,"current_language");
        if(current_language!=null){
            Locale locale = new Locale(current_language);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }else{
            Locale locale = new Locale("fr");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
        setContentView(R.layout.activity_standart);
        setVF(0);
        idb=new infoscolDB(this);
        user=(EditText)findViewById(R.id.ET_login_tel);
        pass=(EditText)findViewById(R.id.ET_login_pass);
        String u=loadSetting(this,"user");
        String p=loadSetting(this,"pass");

        if(u!=null){
            user.setText(u);
        }
        if(p!=null){
            pass.setText(p);
        }
        defaultColorId = user.getDrawingCacheBackgroundColor();


        tv_error=(TextView)findViewById(R.id.TV_login_error);
        tv_error.setTextSize(13);
        conn=(Button)findViewById(R.id.BT_login_conn);

        setTitle(getText(R.string.connection));
        subHost="getstudentinfos";
        filter="com.example.joel.infoscol.login";

        startService(new Intent(this, backgroundChecker.class));


        conn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String AOD = loadSetting(LoginActivity.this, "app_outDated");
                if (AOD != null) {
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle(getString(R.string.maj))
                            .setMessage(getString(R.string.maj_msg))
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();

                                }
                            }).show();
                } else {


                    if (formIsOk(user.getText().toString(), pass.getText().toString())) {
                        String logf = loadSetting(LoginActivity.this, "logInfo");
                        if (logf != null) {
                            String u = loadSetting(LoginActivity.this, "user");
                            String p = loadSetting(LoginActivity.this, "pass");
                            if (u.equals(user.getText().toString())) {
                                if (p.equals(pass.getText().toString())) {
                                    Intent i = new Intent(LoginActivity.this, ActivityStudent.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    tv_error.setText(getText(R.string.incorrect_pass));
                                }
                            } else {
                                opStarted = true;
                                loadingService.networking = true;
                                String filter2 = "com.app.show_loading";
                                IntentFilter itf2 = new IntentFilter(filter2);
                                registerReceiver(refill2, itf2);
                                pushRequest(onResponseReceive, subHost, post, filter, LoginActivity.this);
                                Intent timeOut = new Intent(LoginActivity.this, timeOut_showLoading.class);
                                timeOut.putExtra("filter", filter);
                                startService(timeOut);

                            }
                        } else {
                            opStarted = true;
                            loadingService.networking = true;
                            String filter2 = "com.app.show_loading";
                            IntentFilter itf2 = new IntentFilter(filter2);
                            registerReceiver(refill2, itf2);
                            pushRequest(onResponseReceive, subHost, post, filter, LoginActivity.this);
                            Intent timeOut = new Intent(LoginActivity.this, timeOut_showLoading.class);
                            timeOut.putExtra("filter", filter);
                            startService(timeOut);

                        }

                    }
                    //startActivity(new Intent(LoginActivity.this, ActivityStudent.class));
                }
            }
        });
    }



    boolean formIsOk(String username,String password) {
        boolean val = true;
        char[] pseudo = username.toCharArray();

        tv_error.setText("");
        user.setBackgroundColor(defaultColorId);
        pass.setBackgroundColor(defaultColorId);


        if (username.length() < 4) {
            tv_error.setText(getText(R.string.short_number));
            user.setBackgroundColor(getResources().getColor(R.color.error_text));
            user.requestFocus();
            return false;
        }
        for (int i = 0; i < pseudo.length; i++) {
            int c = pseudo[i];
            if (!between(c, 48, 57)) {
                tv_error.setText(getText(R.string.incorrect_number));
                user.setBackgroundColor(getResources().getColor(R.color.error_text));
                user.requestFocus();
                return false;
            }

        }



        if (password.length() < 5) {
            tv_error.setText(getText(R.string.short_password));
            pass.setBackgroundColor(getResources().getColor(R.color.error_text));
            pass.requestFocus();
            return false;
        }




        post = "pseudo="+username + "&password=" + password ;
        return val;
    }

    boolean between(int val, int min, int max) {
        boolean rep = false;

        if (val >= min && val <= max)
            rep = true;

        return rep;
    }

    void handleRes(String res){
        loadingService.networking=false;
        try {

            JSONObject o= new JSONObject(res);
            String s=o.getString("success");
            saveSetting(this,"logInfo","true");
            saveSetting(this, "user", user.getText().toString());
            saveSetting(this, "pass", pass.getText().toString());
            ///////
            idb.RAZ();

            JSONArray eleves=new JSONArray(o.getString("eleves"));
            for(int i=0;i<eleves.length();i++){
                eleve e =new eleve(eleves.getJSONObject(i));
                Log.e("eleve",e.getNom());
                idb.createEleve(e);
            }
            JSONArray inscriptions=new JSONArray(o.getString("inscriptions"));
            for(int i=0;i<inscriptions.length();i++){
                inscription ins =new inscription(inscriptions.getJSONObject(i));
                idb.createInscription(ins);
            }
            JSONArray scolarites=new JSONArray(o.getString("reglement_scolarites"));
            for(int i=0;i<scolarites.length();i++){
                scolarite scol =new scolarite(scolarites.getJSONObject(i));
                idb.createScolarite(scol);
            }
            JSONArray evaluations=new JSONArray(o.getString("evaluations"));
            for(int i=0;i<evaluations.length();i++){
                evaluation eval =new evaluation(evaluations.getJSONObject(i));
                idb.createEvaluation(eval);
            }
            JSONArray annonces=new JSONArray(o.getString("annonces"));
            for(int i=0;i<annonces.length();i++){
                annonce eval =new annonce(annonces.getJSONObject(i));
                idb.createAnnonce(eval);
            }


            Intent i=new Intent(LoginActivity.this,ActivityStudent.class);
            startActivity(i);
            finish();
        } catch (JSONException e) {
            Log.e("erreur",e.getMessage());
            tv_error.setText(getText(R.string.echec_connection));

            Log.e("erreur", "", e.fillInStackTrace());
        }
    }

    private BroadcastReceiver onResponseReceive= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            stopService(network);

            unregisterReceiver(this);
            String res=intent.getStringExtra("result");
            // ICI gestion d'erreurs sur la reponse
            handleRes(res);



        }
    };
}
