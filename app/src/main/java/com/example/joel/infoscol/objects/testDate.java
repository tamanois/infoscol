package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class testDate {

    String date;
    String classe;

    public testDate(JSONObject jo){
        try{
            date=jo.getString("date");
            classe=jo.getString("classe");
        }catch (Exception e){
            Log.e("testDate",e.getMessage(),e.getCause());
        }
    }

    public String getDate() {
        return date;
    }

    public String getClasse() {
        return classe;
    }
}
