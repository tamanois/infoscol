package com.example.joel.infoscol.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by joel on 14/03/2017.
 */
public class loadingService extends IntentService {
    public static boolean networking=false;


    public loadingService() {
        super("");
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int time=0;
        final int maxLoading=40000;
        try {
            while(networking){

                Thread.sleep(300);
                time+=300;
                if(time>=maxLoading)    break;

            }

            Intent i = new Intent();
            i.setAction("com.hotplaces.networking.finished");
            sendBroadcast(i);

        } catch (Exception e) {

        }
    }
}
