package com.example.joel.infoscol;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.joel.infoscol.database.infoscolDB;
import com.example.joel.infoscol.objects.inscription;
import com.example.joel.infoscol.redefined.adapters.customImageList;

import java.util.ArrayList;

public class DateActivity extends StandartActivity {
    TextView TV;
    String res="[{\"date\":\"2013-2014\",\"classe\":\"4ème\"},{\"date\":\"2014-2015\",\"classe\":\"4ème\"},{\"date\":\"2015-2016\",\"classe\":\"3ème\"}]";
    ArrayList<inscription> inscriptions;
    ArrayList<Integer> img_list ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standart);
        setVF(2);
        final String student_name=getIntent().getStringExtra("student_name");
        setTitle(student_name);
        final int id_eleve=getIntent().getIntExtra("id_eleve",0);
        idb=new infoscolDB(this);
        inscriptions = idb.readInscriptionsByEleve(id_eleve);

        myListView=(ListView)findViewById(R.id.date_list);
        TV=(TextView)findViewById(R.id.TV_date_list_error);
        subHost="something.php";
        post="data=someData";
        filter="com.example.joel.infoscol.objects.testStudent";
        fill_myListView(res);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=new Intent(DateActivity.this,OptionsActivity.class);
                String annee=inscriptions.get(position).getAnnee_scolaire();
                i.putExtra("student_name",student_name);
                i.putExtra("annee",annee);
                i.putExtra("id_eleve",id_eleve);
                startActivity(i);
            }
        });
    }

    public void fill_myListView(String res){
        ArrayList<String>ins=get_date_list(inscriptions);
        customImageList cIL=new customImageList(this,img_list,ins);
        myListView.setAdapter(cIL);
        if(myListView.getCount()<=0)
            TV.setText(getText(R.string.empty_list));
    }


    private BroadcastReceiver onResponseReceive= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stopService(network);
            unregisterReceiver(this);
            String res=intent.getStringExtra("result");
            // ICI gestion d'erreurs sur la reponse

            fill_myListView(res);



        }
    };

    public ArrayList<String> get_date_list(ArrayList<inscription> at){
        ArrayList<String> date=new ArrayList<>();
        img_list=new ArrayList<>();
        int i=1;
        for(inscription j : at) {
            date.add("  " + j.getAnnee_scolaire() + " ( " + j.getClasse() + " )");
            img_list.add(R.drawable.annee_scolaire);
            i++;
        }
        return date;
    }
}
