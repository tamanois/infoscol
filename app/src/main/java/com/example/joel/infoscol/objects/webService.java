package com.example.joel.infoscol.objects;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by joel on 01/03/2017.
 */
public class webService {
    public static String server = "http://panel.wedijit.com/GESCOLAPP/public/";

    String reponse;
    Context c;
    boolean jobDone = false;
    static boolean busy = false;

    public webService() {

    }

    public webService(Context c) {
        this.c = c;
        String ip=loadSetting("ip_address");
        if(ip!=null){
            if(ip.length()>7)
                server=ip;
        }
        else{
            server = "http://panel.wedijit.com/GESCOLAP/public/";
        }
    }

    public String request(final String subHost, final String post) {

        jobDone = false;
        final int maxRetry = 3;
        Thread T1 = new Thread(new Runnable() {


            @Override
            public void run() {
                int t = 0;
                while (t < maxRetry) {
                    jobDone = false;
                    String ur = server + subHost;

                    String postRequest = post;
                    URL url;
                    reponse = "";
                    String ligne = "";
                    URLConnection conn;
                    try {

                        Log.e("connection_start", ur);
                        url = new URL(ur);
                        conn = url.openConnection();
                        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
                        conn.setDoOutput(true);
                        conn.setReadTimeout(10000);
                        conn.setConnectTimeout(10000);
                        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
                        writer.write(postRequest);
                        writer.flush();
                        //recuperation du code html

                        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        int i = 0;
                        while ((ligne = reader.readLine()) != null) {
                            if (i > 0) reponse += "\n";
                            reponse += ligne;

                            t++;
                        }

                        break;

                    } catch (Exception e) {
                        Log.e("Server_error", e.toString());
                        //e.printStackTrace();
                        reponse = e.toString();
                        e.printStackTrace();
                        t++;
                        continue;
                    }

                }
                jobDone = true;


            }
        });
        T1.start();

        while (!jobDone) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return reponse;
    }


    public void saveSetting(String name, String value) {
        SharedPreferences settings;
        settings = c.getSharedPreferences("settings", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public String loadSetting(String name) {
        String value;
        SharedPreferences settings;
        settings = c.getSharedPreferences("settings", 0);
        value = settings.getString(name, null);
        return value;

    }
}

