package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class testAnnonce {

    String date;
    String msg;
    String sender;

    public testAnnonce(JSONObject jo){
        try{
            date=jo.getString("date");
            msg=jo.getString("msg");
            sender=jo.getString("sender");
        }catch (Exception e){
            Log.e("testDate",e.getMessage(),e.getCause());
        }
    }

    public String getDate() {
        return date;
    }

    public String getMsg() {
        return msg;
    }

    public String getSender() {
        return sender;
    }
}
