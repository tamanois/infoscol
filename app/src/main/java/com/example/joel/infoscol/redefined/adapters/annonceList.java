package com.example.joel.infoscol.redefined.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.joel.infoscol.R;
import com.example.joel.infoscol.objects.annonce;

import java.util.ArrayList;


public class annonceList extends ArrayAdapter{

    public final Activity context;
    ArrayList<annonce> annonce_list;
    public annonceList(Activity context, ArrayList<annonce> annonce_list) {
        super(context, R.layout.annonces_listview, annonce_list);
        this.annonce_list=new ArrayList<>();
        this.context = context;

            this.annonce_list=annonce_list;

    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.annonces_listview, null, true);
        TextView tv_date = (TextView) rowView.findViewById(R.id.date_annonce);
        TextView tv_msg = (TextView) rowView.findViewById(R.id.msg_annonce);
        TextView tv_sender = (TextView) rowView.findViewById(R.id.sender_annonce);

       tv_date.setText(annonce_list.get(position).getDate());
        tv_msg.setText(annonce_list.get(position).getMsg());
        tv_sender.setText(annonce_list.get(position).getSender());



        return rowView;
    }


}
