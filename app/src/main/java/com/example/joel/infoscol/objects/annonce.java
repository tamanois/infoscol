package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class annonce {

    int id;
    String annee_scolaire;
    int code_etablissement;
    String classe;
    String msg;
    String date;
    String sender;
    String created_at;

    public annonce(JSONObject jo) {
        try {
            id = jo.getInt("id");
            annee_scolaire = jo.getString("annee_scolaire");
            annee_scolaire=annee_scolaire.replace("-"," - ");
            code_etablissement = jo.getInt("code_etablissement");
            classe = jo.getString("destinataires");
            date = jo.getString("date_annonce");
            msg = jo.getString("intitule_annonce");
            sender = jo.getString("emetteur");
            created_at = jo.getString("created_at");

        } catch (Exception e) {
            Log.e("annonce", e.getMessage(), e.getCause());
        }
    }

    public annonce(int id, String annee_scolaire, int code_etablissement, String classe, String msg, String date, String sender) {
        this.id = id;
        this.annee_scolaire = annee_scolaire;
        this.code_etablissement = code_etablissement;
        this.classe = classe;
        this.msg = msg;
        this.date = date;
        this.sender = sender;
    }

    public int getId() {
        return id;
    }

    public String getAnnee_scolaire() {
        return annee_scolaire;
    }

    public int getCode_etablissement() {
        return code_etablissement;
    }

    public String getClasse() {
        return classe;
    }

    public String getMsg() {
        return msg;
    }

    public String getDate() {
        return date;
    }

    public String getSender() {
        return sender;
    }

    public String getCreated_at() {
        return created_at;
    }
}
