package com.example.joel.infoscol.objects;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by Joel on 30/07/2017.
 */
public class testScolarite {

    double pension;
    String date;
    double montant ;
    String limite_versement;

    public testScolarite(JSONObject jo){
        try{
            pension=jo.getDouble("pension");
            date=jo.getString("date");
            montant=jo.getDouble("montant");
            limite_versement=jo.getString("limite_versement");
        }catch (Exception e){
            Log.e("testDate",e.getMessage(),e.getCause());
        }
    }

    public String getDate() {
        return date;
    }

    public double getPension() {
        return pension;
    }

    public double getMontant() {
        return montant;
    }

    public String getLimite_versement() {
        return limite_versement;
    }
}
